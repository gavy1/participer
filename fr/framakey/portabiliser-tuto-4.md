# Tester sa portabilité

Votre logiciel est discret. Parfait !

Prenons alors le répertoire où vous avez décompressé PNotes et mettons 
le sur une clé USB de sorte à avoir `F:\PNotes` (où `F:\` est le disque 
représentant votre clé USB)

Si vous avez un autre poste, l’idéal est alors d’aller sur l’autre 
poste afin de mettre la clé USB dessus pour faire les tests.

Si vous n’avez pas d’autre poste sous la main et que vous aviez installé 
le logiciel que vous essayez de portabiliser, désinstallez le.

Les tests vont consister à tester le logiciel en le lançant à partir de 
la clé USB. 
Lors de l’utilisation du logiciel à partir de la clé USB, 
testez un maximum de fonctionnalité en n’oubliant pas de modifier 
et sauvegarder des préférences ou des options.

Une fois terminé, fermez l’application, retirez votre clé USB et allez 
sur un autre poste (par exemple le poste où vous aviez fait les premiers 
tests de discrétion). Insérez la clé USB et lancez le logiciel à partir 
de la clé USB.

Vérifiez que les options modifiés sont toujours sélectionnés. Si c’est 
le cas et que vous n’avez rencontré aucune difficulté au niveau de 
l’utilisation du logiciel, vous pouvez considérer le logiciel testé 
comme portable.

Prêts pour réaliser le lanceur ? [Allons-y…](portabiliser-tuto-5.html)  
[Retourner à la page précédente…](portabiliser-tuto-3.html)