# Préambule
## A propos de ce tutoriel

 *  Objectif : Etre en mesure de packager des applications libres 
    portables pour compléter le portail Framakey ou les ajouter aux
    packages existants. Plus globalement : savoir portabiliser une application.
 *  Auteur : pyg, Léviathan
 *  Licence : GFDL, Art Libre
 *  Niveau requis : bidouilleur persévérant
 *  Temps nécessaire : 4 bonnes heures 

## C’est quoi un logiciel portable ?

Ça peut paraitre bête, mais autant re-préciser ici ce qu’est, selon 
nous un logiciel « portable »…
Il s’agit d’un logiciel qui fonctionne en « vase clos » : il est 
exécuté dans un dossier et fonctionne de façon autonome dans ce dernier. 
Ainsi si on déplace ce dernier (par exemple sur une clé usb), le 
logiciel se suffit à lui-même et n’a besoin que du système 
d’exploitation (dans notre cas Windows) pour fonctionner.

## Pourquoi portabiliser un logiciel ?

Parce que c’est fun :P
Plus sérieusement, nous avons tous l’occasion de travailler sur 
différents ordinateurs et il serait bien pratique de pouvoir 
transporter avec vous vos logiciels comme vous transportez vos documents. 
C’est exactement ce que permettent les logiciels portables. 
Vous retrouvez toute votre configuration, quelque soit la machine sur 
laquelle vous exécutez le logiciel.

Nous pensons sincèrement que tout le monde (développeurs et utilisateurs) 
gagnerait à n’utiliser que des logiciels portables. Pour les techniciens, 
ela facilite le backup, les mises à jours, les installations, etc. 
Pour l’utilisateur, cela lui fait gagner du temps et de la liberté.

## Qui peut portabiliser un logiciel ?

Ca dépend plus du logiciel que de la personne ! Ainsi, dans le cas présenté ici, 
es compétences informatiques demandées sont vraiment rudimentaires. 
Le profil est plus celui d’un bidouilleur que d’un développeur.

A ce stade, il est important de comprendre que le processus de 
portabilisation ne modifie **en rien** le logiciel original. 
C’est à dire qu’on ne touche pas à une seule ligne de son code source, 
on va juste le « préparer » pour le l’exécuter d’une façon particulière.

Pour être encore plus clair, l’immense majorité des logiciels disponibles 
dans le portail [Framakey](https://dev.framakey.org/application-portables-libres) 
étaient **déjà** portables. 
À quelques exceptions près (logiciels Mozilla ou OpenOffice.org), le
travail de l’équipe Framakey à plus été un travail de « labelisation »
qu’un travail de « portabilisation ».

## Pourquoi un « label » Framakey ?

Label est un bien grand mot. Disons que nous souhaitons proposer des 
logiciels libres portables de qualité, dont nous avons pu vérifier la 
portabilité, avant de les soumettre à nos visiteurs. Pour cela, puisque 
nous les diffusons, nous les vérifions/testons/validons avant de les 
« lâcher dans la nature ».
Enfin, cela permet de donner de la cohérence aux packages Framakey, et 
permet d’anticiper de futurs usages que nous préparons en douce depuis 
plusieurs mois :)

Ainsi, comprenez bien qu’un logiciel comme portableFirefox est basé à 
100% sur le code de Firefox. 
Nous avons juste adjoint un fichier `portableFirefox.exe` qui permet 
d’éxecuter Firefox sur média amovible. 
Si vous appréciez portableFirefox, c’est donc la communauté Mozilla/Firefox 
qu’il faut féliciter pour les milliers d’heures passées à développer ce 
fabuleux logiciel, et non la communauté Framakey qui ne propose, 
très modestement, qu’une autre façon de l’utiliser.

Pour utiliser une métaphore, disons que le travail de portabilisation, 
c’est un peu comme mettre un logiciel dans un sac à dos. il y a des 
logiciels qui vont rentrer sans problème, et d’autres qui ne rentreront 
jamais. 
Mais il ne faut pas mélanger : Framakey s’occupe du sac, mais le 
sandwich/logiciel qu’on met dedans, ce n’est pas nous qui l’avons fait. 
(quoi ? elle est pourrie, ma métaphore ? )

Mais assez parlé, vous êtes là pour portabiliser un logiciel ? Alors allons-y !

## Trouver le logiciel à portabiliser

Bon, là ça se complique un peu : pour être franc avec vous certains 
logiciels sont infiniment plus simple à portabiliser que d’autres. 
Ainsi certains logiciels sont « nativement » portables, et d’autres ne 
pourront que très difficilement l’être.

Certains critères peuvent vous mettre sur la piste d’un logiciel facile 
à portabiliser :

 *  le logiciel est-il libre ? Si oui, c’est bon signe, parce que les 
    développeurs de logiciels libres aiment à développer pour plusieurs 
    plateformes (Linux, Mac, Windows). Cela signifie qu’il y a de fortes 
    chance pour que le logiciel n’utilisent pas des spécificités 
    windowsiennes comme la « base de registre » qui pose problème 
    lors de la portabilisation.
 *  le logiciel est-il disponible au téléchargement en version zippée ?
    Si oui, c’est bon signe. Cela signifie qu’il n’installe probablement 
    pas des spécificités windowsiennes comme les « fichiers DLL » 
    qui posent problème lors de la portabilisation. 

Dans ce tutoriel, nous allons prendre un cas simple (et on verra pour 
des tutos plus avancés plus tard).
Nous allons portabiliser le logiciel PNotes (qui sert à créer des notes 
ou post-it sur son bureau).

Pourquoi PNotes?

 *  parce qu’il est libre
 *  parce que là maintenant tout de suite, j’ai besoin d’en avoir une 
    version portable que je pourrais transporter sur ma clef USB
 *  parce qu’il est nativement portable (oui, je le savais avant, je triche !) 

Soyons donc tout à fait clair : PNotes est en fait déjà portable. 
Nous pourrions le copier directement sur notre clé USB et nous en servir 
sans autre forme de procès.

Ce premier tutoriel est destiné à vous montrer, dans les grandes lignes, 
comment portabiliser un logiciel pour la Framakey. Pour l’exemple, 
nous avons donc retenu un logiciel qui ne posait pas de problème de 
portabilisation. 

Allez, maintenant, [préparons le terrain…](portabiliser-tuto-1.html)