# Test final

Voilà votre application portable est finalisée.

Il faut maintenant la tester directement sur la clé USB, 
Prenez le répertoire `E:\dev\portables\Pack\DevToolsPortable\FramaWizard-2.0\PNotesPortable\`
et copier le sur votre clé USB afin d’avoir la structure suivante : 
`F:\PNotesPortable` (où `F:\` est la lettre de votre clé USB)

Prenez votre clé USB et faites un maximum de test sur des ordinateurs 
différents en n’oubliant pas qu’il faut dorénavant lancer 
`F:\PNotesPortable\PNotesPortable.exe` pour faire fonctionner l’application.

Si tout ce passe correctement, alors soumettez la à l’équipe de Framakey via [le forum](). 
Nous serions ravi de placer votre application portable sur le site officiel de Framakey.

[Retourner à la page précédente](portabiliser-tuto-5.html)